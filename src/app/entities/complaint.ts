import { User } from './user';
import { Animal } from './animal';
import { Address } from './address';

export class Complaint{

  constructor(){
    this.init();
  }

  init(){
    this.animal = new Animal();
    this.address = new Address();
    this.user = new User();
  }

  user:User;
  animal:Animal;
  address:Address;
  date:string;
  type:string;
  description:string;
}
