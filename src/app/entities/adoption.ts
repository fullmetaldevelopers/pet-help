import { User } from './user';
import { Animal } from './animal';
import { Address } from './address';

export class Adoption{

  constructor(){
    this.init();
  }

  init(){
    this.animal = new Animal();
    this.address = new Address();
    this.user = new User();
  }

  user:User;
  animalName:string;
  animal:Animal;
  animalAge: string;
  address:Address;
  description:string;
}
