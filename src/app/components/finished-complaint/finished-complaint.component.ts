import { Component, OnInit } from '@angular/core';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { Complaint } from '../../entities/complaint';
import { Address } from '../../entities/address';
import { User } from '../../entities/user';
import { ComplaintService } from '../../services/complaint.service';

@Component({
  selector: 'finished-complaint',
  templateUrl: 'finished-complaint.component.html',
})
export class FinishedComplaintComponent implements OnInit {

  complaints: FirebaseListObservable<Complaint[]>;

  constructor(private complaintService: ComplaintService, private angularFire: AngularFire) {  }

  ngOnInit() {
    this.complaints = this.angularFire.database.list('/finishedComplaints',{
      query: {
        orderByChild : 'date'
      }
    });
  }

    showAddress(address: Address){
      alert('Rua: '+address.street+
      '\nCidade: '+address.city+
      '\nEstado: '+address.state);
    }

    showUser(user: User){
      alert('Nome: '+user.name+
      '\nEmail: '+user.email+
      '\nFone: '+user.fone);
    }

    showDescription(description: string){
      alert(description);
    }

}
