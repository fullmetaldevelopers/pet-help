import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AdoptionService } from '../../services/adoption.service';
import { User } from '../../entities/user';
import { Adoption } from '../../entities/adoption';
import { Address } from '../../entities/address';
import { Animal } from '../../entities/animal';

@Component({
  selector: 'register-adoption',
  templateUrl: './register-adoption.component.html'
})
export class RegisterAdoptionComponent implements OnInit {

  animalName: string;
  animalAge: string;
  adoption: Adoption;


  initAdoption(){
    this.adoption = new Adoption();
  }

  constructor(private adoptionService: AdoptionService, private router: Router) {
    this.initAdoption();
  }

  ngOnInit() {}

  registerAdoption(){
    this.adoption.user.name  = 'Renato';
    this.adoption.user.email = 'renato@gmail.com';
    this.adoption.user.fone  = '12345678';
    this.adoption.user.password ='123456';
    this.adoptionService.saveAdoption(this.adoption);
    alert('Adoção salva com sucesso!');

    this.adoption = null;
    this.animalName = ' ';
    this.animalAge = ' ';
    this.router.navigate(['/home']);
  }
}
