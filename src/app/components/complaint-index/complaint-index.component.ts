import { Component, OnInit } from '@angular/core';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { Complaint } from '../../entities/complaint';
import { Address } from '../../entities/address';
import { User } from '../../entities/user';
import { ComplaintService } from '../../services/complaint.service';

@Component({
  selector: 'complaint-index',
  templateUrl: './complaint-index.component.html'
})
export class ComplaintIndexComponent implements OnInit {

  complaints: FirebaseListObservable<Complaint[]>;

  constructor(private complaintService: ComplaintService, private angularFire: AngularFire) { }

  ngOnInit() {
    this.complaints = this.angularFire.database.list('/complaints',{
      query: {
        orderByChild : 'date'
      }
    });
  }

  showAddress(address: Address){
    alert('Rua: '+address.street+
    '\nCidade: '+address.city+
    '\nEstado: '+address.state);
  }

  showUser(user: User){
    alert('Nome: '+user.name+
    '\nEmail: '+user.email+
    '\nFone: '+user.fone);
  }

  showDescription(description: string){
    alert(description);
  }

  finishComplaint(complaint: Complaint, key:string){
      this.complaintService.finishComplaint(complaint, key);
  }
}
