import { Component, OnInit } from '@angular/core';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { Adoption } from '../../entities/adoption';
import { Address } from '../../entities/address';
import { User } from '../../entities/user';
import { AdoptionService } from '../../services/adoption.service';

@Component({
  selector: 'adopted-index',
  templateUrl: 'adopted-index.component.html',
})
export class AdoptedIndexComponent implements OnInit {

  adoptions: FirebaseListObservable<Adoption[]>;

  constructor(private adoptionService:AdoptionService, private angularFire: AngularFire) {
  }

  ngOnInit() {
    this.adoptions = this.angularFire.database.list('/adopteds',{
      query: {
        orderByChild : 'animalName'
      }
    });
  }

  showAddress(address: Address){
    alert('Rua: '+address.street+
    '\nCidade: '+address.city+
    '\nEstado: '+address.state);
  }

  showUser(user: User){
    alert('Nome: '+user.name+
    '\nEmail: '+user.email+
    '\nFone: '+user.fone);
  }

  showDescription(description: string){
    alert(description);
  }

}
