import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';
import { User } from '../../entities/user';

@Component({
  selector: 'register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  user: User;

  confirmPassword:string;

  constructor(private router: Router, private userService: UserService) {
    this.initUser();
  }

  ngOnInit(): void{
  }

  initUser() {
    this.user = new User();
   }

  registerUser(){
    if (this.user.password === this.confirmPassword)  {
      this.userService.createUser(this.user);
      this.router.navigate(['/login']);
    }else{
      alert('Senhas não são iguais!');
    }
  }

}
