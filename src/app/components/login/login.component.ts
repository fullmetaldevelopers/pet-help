import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { User } from '../../entities/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;

  constructor(private userService: UserService, private router:  Router, private af: AngularFire) {
    this.initUser();
  }

  initUser() {
    this.user = new User();
   }

  ngOnInit() {
  }

  doLogin(){
    this.userService.doLogin(this.user.email, this.user.password);
    this.router.navigate(['/home']);
  }

  registerUser(){
    this.router.navigate(['/register-user']);
  }

}
