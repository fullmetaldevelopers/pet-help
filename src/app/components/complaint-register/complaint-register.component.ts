import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ComplaintService } from '../../services/complaint.service';
import { Complaint } from '../../entities/complaint';
import { User } from '../../entities/user';
import { Address } from '../../entities/address';
import { Animal } from '../../entities/animal';

@Component({
  selector: 'complaint-register',
  templateUrl: './complaint-register.component.html'
})
export class ComplaintRegisterComponent implements OnInit {

  complaint: Complaint;

  ngOnInit() {

  }

  initComplaint(){
    this.complaint = new Complaint();
  }

  constructor(private complaintService: ComplaintService, private router: Router) {
      this.initComplaint();
  }

  registerComplaint(){
    this.complaint.user.name  = 'Renato';
    this.complaint.user.email = 'renato@gmail.com';
    this.complaint.user.fone  = '12345678';
    this.complaint.user.password ='123456';
    this.complaintService.saveComplaint(this.complaint);

      alert('Denúncia salva com sucesso!');
    this.complaint = null;

    this.router.navigate(['/home']);

    }
  }
