import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ComplaintRegisterComponent } from './components/complaint-register/complaint-register.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { RegisterAdoptionComponent } from './components/register-adoption/register-adoption.component';
import { ComplaintIndexComponent } from './components/complaint-index/complaint-index.component';
import { AdoptionIndexComponent } from './components/adoption-index/adoption-index.component';
import { AdoptedIndexComponent } from './components/adopted-index/adopted-index.component';
import { FinishedComplaintComponent } from './components/finished-complaint/finished-complaint.component';

const ROUTES: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'home'              , component: HomeComponent},
  {path: 'complaint-register', component: ComplaintRegisterComponent},
  {path: 'login'             , component: LoginComponent},
  {path: 'register-user'     , component: RegisterUserComponent},
  {path: 'register-adoption' , component: RegisterAdoptionComponent},
  {path: 'complaint-index'   , component: ComplaintIndexComponent},
  {path: 'adoption-index'    , component: AdoptionIndexComponent},
  {path: 'adopted-index'     , component: AdoptedIndexComponent},
  {path: 'finished-complaint', component: FinishedComplaintComponent}
]


@NgModule({
  imports:[RouterModule.forRoot(ROUTES)],
  exports:[RouterModule]
})

export class AppRouterModule {}
