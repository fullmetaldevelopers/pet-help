import { Injectable } from '@angular/core';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { Complaint } from '../entities/complaint';

@Injectable()
export class ComplaintService {

  complaints: FirebaseListObservable<Complaint[]>;
  finishedComplaints : FirebaseListObservable<Complaint[]>;

  constructor(private angularFire: AngularFire) {
    this.complaints = this.angularFire.database.list('/complaints',{
      query: {
        orderByChild : 'date'
      }
    });
    this.finishedComplaints = this.angularFire.database.list('/finishedComplaints',{
      query: {
        orderByChild : 'date'
      }
    });
  }

  saveComplaint(complaint:Complaint){
    this.complaints.push(complaint);
  }

  finishComplaint(complaint:Complaint, key:string){
    delete (<any>complaint).$key;
    let updatingComplaint = JSON.stringify(complaint);
    complaint = JSON.parse(updatingComplaint);
    this.finishedComplaints.push(complaint);
    this.complaints.remove(key);
  }

}
