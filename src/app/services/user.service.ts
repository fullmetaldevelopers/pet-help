import { Injectable } from '@angular/core';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { User } from '../entities/user';

@Injectable()
export class UserService {

  users: FirebaseListObservable<User[]>;

  constructor(private angularFire: AngularFire) {
    this.users = this.angularFire.database.list('/users',{
      query: {
        orderByChild : 'name'
      }
    });
  }

  doLogin(email:string, password:string){
      this.angularFire.auth.login({email: email, password: password});
  }

  removeUser(key: string) {
      this.users.remove(key);
    }

  createUser(user:User){
    this.users.push(user);
    this.angularFire.auth.createUser({email: user.email, password: user.password});
  }
}
