import { Injectable } from '@angular/core';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { Adoption } from '../entities/adoption';

@Injectable()
export class AdoptionService {

  adoptions: FirebaseListObservable<Adoption[]>;
  adopteds: FirebaseListObservable<Adoption[]>;

  constructor(private angularFire: AngularFire) {
    this.adoptions = this.angularFire.database.list('/adoptions',{
      query: {
        orderByChild : 'animalName'
      }
    });
    this.adopteds = this.angularFire.database.list('/adopteds',{
      query: {
        orderByChild : 'animalName'
      }
    });
  }

  saveAdoption(adoption:Adoption){
    this.adoptions.push(adoption);
  }

  updateAdoption(adoption:Adoption){
  }

  adopt(adoption:Adoption, key:string){
    delete (<any>adoption).$key;
    let updatingAdoption = JSON.stringify(adoption);
    adoption = JSON.parse(updatingAdoption);
    this.adopteds.push(adoption);
    this.adoptions.remove(key);
  }
}
