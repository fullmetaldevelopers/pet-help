import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule} from '@angular/router';

import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { AppRouterModule } from './approuter.module';
import { AppComponent } from './app.component';
import { ComplaintRegisterComponent } from './components/complaint-register/complaint-register.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { RegisterAdoptionComponent } from './components/register-adoption/register-adoption.component';
import { ComplaintIndexComponent } from './components/complaint-index/complaint-index.component';
import { AdoptionIndexComponent } from './components/adoption-index/adoption-index.component';
import { AdoptedIndexComponent } from './components/adopted-index/adopted-index.component';
import { FinishedComplaintComponent } from './components/finished-complaint/finished-complaint.component';
import { AdministratorService } from './services/administrator.service';
import { UserService } from './services/user.service';
import { AdoptionService } from './services/adoption.service';
import { ComplaintService } from './services/complaint.service';

export const myFireBaseConfig = {
    apiKey: "AIzaSyD1tBvyfRd61rD6RCFRdqnIkNhQ2zct3ns",
    authDomain: "pet-help-f2696.firebaseapp.com",
    databaseURL: "https://pet-help-f2696.firebaseio.com",
    storageBucket: "pet-help-f2696.appspot.com",
    messagingSenderId: "878095724222"
};

const firebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    AppComponent,
    ComplaintRegisterComponent,
    HomeComponent,
    LoginComponent,
    RegisterUserComponent,
    RegisterAdoptionComponent,
    ComplaintIndexComponent,
    AdoptionIndexComponent,
    AdoptedIndexComponent,
    FinishedComplaintComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRouterModule,
    AngularFireModule.initializeApp(myFireBaseConfig, firebaseAuthConfig)
  ],
  providers: [
    AdministratorService,
    UserService,
    AdoptionService,
    ComplaintService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
