var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ComplaintRegisterComponent } from './components/complaint-register/complaint-register.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { RegisterAdoptionComponent } from './components/register-adoption/register-adoption.component';
import { ComplaintIndexComponent } from './components/complaint-index/complaint-index.component';
import { AdoptionIndexComponent } from './components/adoption-index/adoption-index.component';
var ROUTES = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'complaint-register', component: ComplaintRegisterComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register-user', component: RegisterUserComponent },
    { path: 'register-adoption', component: RegisterAdoptionComponent },
    { path: 'complaint-index', component: ComplaintIndexComponent },
    { path: 'adoption-index', component: AdoptionIndexComponent }
];
var AppRouterModule = (function () {
    function AppRouterModule() {
    }
    return AppRouterModule;
}());
AppRouterModule = __decorate([
    NgModule({
        imports: [RouterModule.forRoot(ROUTES)],
        exports: [RouterModule]
    }),
    __metadata("design:paramtypes", [])
], AppRouterModule);
export { AppRouterModule };
//# sourceMappingURL=../../../src/app/app-router.module.js.map