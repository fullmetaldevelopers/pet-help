import { User } from './user';
import { Animal } from './animal';
import { Address } from './address';
export var Adoption = (function () {
    function Adoption() {
        this.init();
    }
    Adoption.prototype.init = function () {
        this.animal = new Animal();
        this.address = new Address();
        this.user = new User();
    };
    return Adoption;
}());
//# sourceMappingURL=../../../../src/app/entities/adoption.js.map