import { User } from './user';
import { Animal } from './animal';
import { Address } from './address';
export var Complaint = (function () {
    function Complaint() {
        this.init();
    }
    Complaint.prototype.init = function () {
        this.animal = new Animal();
        this.address = new Address();
        this.user = new User();
    };
    return Complaint;
}());
//# sourceMappingURL=../../../../src/app/entities/complaint.js.map