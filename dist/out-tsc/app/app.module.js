var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { AppRouterModule } from './approuter.module';
import { AppComponent } from './app.component';
import { ComplaintRegisterComponent } from './components/complaint-register/complaint-register.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { RegisterAdoptionComponent } from './components/register-adoption/register-adoption.component';
import { ComplaintIndexComponent } from './components/complaint-index/complaint-index.component';
import { AdoptionIndexComponent } from './components/adoption-index/adoption-index.component';
import { AdoptedIndexComponent } from './components/adopted-index/adopted-index.component';
import { FinishedComplaintComponent } from './components/finished-complaint/finished-complaint.component';
import { AdministratorService } from './services/administrator.service';
import { UserService } from './services/user.service';
import { AdoptionService } from './services/adoption.service';
import { ComplaintService } from './services/complaint.service';
export var myFireBaseConfig = {
    apiKey: "AIzaSyD1tBvyfRd61rD6RCFRdqnIkNhQ2zct3ns",
    authDomain: "pet-help-f2696.firebaseapp.com",
    databaseURL: "https://pet-help-f2696.firebaseio.com",
    storageBucket: "pet-help-f2696.appspot.com",
    messagingSenderId: "878095724222"
};
var firebaseAuthConfig = {
    provider: AuthProviders.Password,
    method: AuthMethods.Password
};
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    NgModule({
        declarations: [
            AppComponent,
            ComplaintRegisterComponent,
            HomeComponent,
            LoginComponent,
            RegisterUserComponent,
            RegisterAdoptionComponent,
            ComplaintIndexComponent,
            AdoptionIndexComponent,
            AdoptedIndexComponent,
            FinishedComplaintComponent
        ],
        imports: [
            BrowserModule,
            FormsModule,
            HttpModule,
            AppRouterModule,
            AngularFireModule.initializeApp(myFireBaseConfig, firebaseAuthConfig)
        ],
        providers: [
            AdministratorService,
            UserService,
            AdoptionService,
            ComplaintService
        ],
        bootstrap: [AppComponent]
    }),
    __metadata("design:paramtypes", [])
], AppModule);
export { AppModule };
//# sourceMappingURL=../../../src/app/app.module.js.map