import { TestBed, inject } from '@angular/core/testing';
import { UserService } from './user-service.service';
describe('UserService', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            providers: [UserService]
        });
    });
    it('should ...', inject([UserService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=../../../../src/app/services/user-service.service.spec.js.map