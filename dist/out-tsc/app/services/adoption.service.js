var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
var AdoptionService = (function () {
    function AdoptionService(angularFire) {
        this.angularFire = angularFire;
        this.adoptions = this.angularFire.database.list('/adoptions', {
            query: {
                orderByChild: 'animalName'
            }
        });
        this.adopteds = this.angularFire.database.list('/adopteds', {
            query: {
                orderByChild: 'animalName'
            }
        });
    }
    AdoptionService.prototype.saveAdoption = function (adoption) {
        this.adoptions.push(adoption);
    };
    AdoptionService.prototype.updateAdoption = function (adoption) {
    };
    AdoptionService.prototype.adopt = function (adoption, key) {
        delete adoption.$key;
        var updatingAdoption = JSON.stringify(adoption);
        adoption = JSON.parse(updatingAdoption);
        this.adopteds.push(adoption);
        this.adoptions.remove(key);
    };
    return AdoptionService;
}());
AdoptionService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [AngularFire])
], AdoptionService);
export { AdoptionService };
//# sourceMappingURL=../../../../src/app/services/adoption.service.js.map