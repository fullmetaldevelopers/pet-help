var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ComplaintService } from '../../services/complaint.service';
import { Complaint } from '../../entities/complaint';
var ComplaintRegisterComponent = (function () {
    function ComplaintRegisterComponent(complaintService, router) {
        this.complaintService = complaintService;
        this.router = router;
        this.initComplaint();
    }
    ComplaintRegisterComponent.prototype.ngOnInit = function () {
    };
    ComplaintRegisterComponent.prototype.initComplaint = function () {
        this.complaint = new Complaint();
    };
    ComplaintRegisterComponent.prototype.registerComplaint = function () {
        this.complaint.user.name = 'Renato';
        this.complaint.user.email = 'renato@gmail.com';
        this.complaint.user.fone = '12345678';
        this.complaint.user.password = '123456';
        this.complaintService.saveComplaint(this.complaint);
        alert('Denúncia salva com sucesso!');
        this.complaint = null;
        this.router.navigate(['/home']);
    };
    return ComplaintRegisterComponent;
}());
ComplaintRegisterComponent = __decorate([
    Component({
        selector: 'complaint-register',
        templateUrl: './complaint-register.component.html'
    }),
    __metadata("design:paramtypes", [ComplaintService, Router])
], ComplaintRegisterComponent);
export { ComplaintRegisterComponent };
//# sourceMappingURL=../../../../../src/app/components/complaint-register/complaint-register.component.js.map