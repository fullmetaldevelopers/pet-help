var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { AdoptionService } from '../../services/adoption.service';
var AdoptionIndexComponent = (function () {
    function AdoptionIndexComponent(adoptionService, angularFire) {
        this.adoptionService = adoptionService;
        this.angularFire = angularFire;
    }
    AdoptionIndexComponent.prototype.ngOnInit = function () {
        this.adoptions = this.angularFire.database.list('/adoptions', {
            query: {
                orderByChild: 'animalName'
            }
        });
    };
    AdoptionIndexComponent.prototype.showAddress = function (address) {
        alert('Rua: ' + address.street +
            '\nCidade: ' + address.city +
            '\nEstado: ' + address.state);
    };
    AdoptionIndexComponent.prototype.showUser = function (user) {
        alert('Nome: ' + user.name +
            '\nEmail: ' + user.email +
            '\nFone: ' + user.fone);
    };
    AdoptionIndexComponent.prototype.showDescription = function (description) {
        alert(description);
    };
    AdoptionIndexComponent.prototype.adopt = function (adoption, key) {
        this.adoptionService.adopt(adoption, key);
    };
    return AdoptionIndexComponent;
}());
AdoptionIndexComponent = __decorate([
    Component({
        selector: 'adoption-index',
        templateUrl: './adoption-index.component.html'
    }),
    __metadata("design:paramtypes", [AdoptionService, AngularFire])
], AdoptionIndexComponent);
export { AdoptionIndexComponent };
//# sourceMappingURL=../../../../../src/app/components/adoption-index/adoption-index.component.js.map