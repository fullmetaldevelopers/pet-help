var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFire } from 'angularfire2';
import { User } from '../../entities/user';
import { UserService } from '../../services/user.service';
var LoginComponent = (function () {
    function LoginComponent(userService, router, af) {
        this.userService = userService;
        this.router = router;
        this.af = af;
        this.initUser();
    }
    LoginComponent.prototype.initUser = function () {
        this.user = new User();
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.doLogin = function () {
        this.userService.doLogin(this.user.email, this.user.password);
        this.router.navigate(['/home']);
    };
    LoginComponent.prototype.registerUser = function () {
        this.router.navigate(['/register-user']);
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Component({
        selector: 'login',
        templateUrl: 'login.component.html',
        styleUrls: ['./login.component.css']
    }),
    __metadata("design:paramtypes", [UserService, Router, AngularFire])
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=../../../../../src/app/components/login/login.component.js.map