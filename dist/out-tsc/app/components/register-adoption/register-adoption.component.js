var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AdoptionService } from '../../services/adoption.service';
import { Adoption } from '../../entities/adoption';
var RegisterAdoptionComponent = (function () {
    function RegisterAdoptionComponent(adoptionService, router) {
        this.adoptionService = adoptionService;
        this.router = router;
        this.initAdoption();
    }
    RegisterAdoptionComponent.prototype.initAdoption = function () {
        this.adoption = new Adoption();
    };
    RegisterAdoptionComponent.prototype.ngOnInit = function () { };
    RegisterAdoptionComponent.prototype.registerAdoption = function () {
        this.adoption.user.name = 'Renato';
        this.adoption.user.email = 'renato@gmail.com';
        this.adoption.user.fone = '12345678';
        this.adoption.user.password = '123456';
        this.adoptionService.saveAdoption(this.adoption);
        alert('Adoção salva com sucesso!');
        this.adoption = null;
        this.animalName = ' ';
        this.animalAge = ' ';
        this.router.navigate(['/home']);
    };
    return RegisterAdoptionComponent;
}());
RegisterAdoptionComponent = __decorate([
    Component({
        selector: 'register-adoption',
        templateUrl: './register-adoption.component.html'
    }),
    __metadata("design:paramtypes", [AdoptionService, Router])
], RegisterAdoptionComponent);
export { RegisterAdoptionComponent };
//# sourceMappingURL=../../../../../src/app/components/register-adoption/register-adoption.component.js.map