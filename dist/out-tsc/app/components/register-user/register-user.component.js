var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../entities/user';
var RegisterUserComponent = (function () {
    function RegisterUserComponent(router, userService) {
        this.router = router;
        this.userService = userService;
        this.initUser();
    }
    RegisterUserComponent.prototype.ngOnInit = function () {
    };
    RegisterUserComponent.prototype.initUser = function () {
        this.user = new User();
    };
    RegisterUserComponent.prototype.registerUser = function () {
        if (this.user.password === this.confirmPassword) {
            this.userService.createUser(this.user);
            this.router.navigate(['/login']);
        }
        else {
            alert('Senhas não são iguais!');
        }
    };
    return RegisterUserComponent;
}());
RegisterUserComponent = __decorate([
    Component({
        selector: 'register-user',
        templateUrl: './register-user.component.html',
        styleUrls: ['./register-user.component.css']
    }),
    __metadata("design:paramtypes", [Router, UserService])
], RegisterUserComponent);
export { RegisterUserComponent };
//# sourceMappingURL=../../../../../src/app/components/register-user/register-user.component.js.map